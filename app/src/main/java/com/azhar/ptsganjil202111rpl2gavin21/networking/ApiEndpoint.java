package com.azhar.ptsganjil202111rpl2gavin21.networking;


public class ApiEndpoint {

    public static String BASEURL = "http://api.themoviedb.org/3/";
    public static String APIKEY = "api_key=859e1e2595ca61e03a724fb8889e0ddb";
    public static String LANGUAGE = "&language=en-US";
    public static String MOVIE_POPULAR = "discover/movie?";
    public static String URLIMAGE = "https://image.tmdb.org/t/p/w780/";
    public static String URLFILM = "https://www.themoviedb.org/movie/";

}
