package com.azhar.ptsganjil202111rpl2gavin21.activities;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.azhar.ptsganjil202111rpl2gavin21.R;
import com.azhar.ptsganjil202111rpl2gavin21.model.ModelMovie;
import com.azhar.ptsganjil202111rpl2gavin21.networking.ApiEndpoint;
import com.azhar.ptsganjil202111rpl2gavin21.realm.RealmHelper;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.ivbaranov.mfb.MaterialFavoriteButton;
import com.google.android.material.snackbar.Snackbar;

public class DetailMovieActivity extends AppCompatActivity {

    TextView tvTitle, tvName, tvOverview;
    ImageView imgPhoto;
    MaterialFavoriteButton imgFavorite;
    String NameFilm, Overview, Thumbnail;
    int Id;
    ModelMovie modelMovie;
    RealmHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        imgPhoto = findViewById(R.id.imgPhoto);
        imgFavorite = findViewById(R.id.imgFavorite);
        tvTitle = findViewById(R.id.tvTitle);
        tvName = findViewById(R.id.tvName);
        tvOverview = findViewById(R.id.tvOverview);

        helper = new RealmHelper(this);

        modelMovie = (ModelMovie) getIntent().getSerializableExtra("detailMovie");
        if (modelMovie != null) {

            Id = modelMovie.getId();
            NameFilm = modelMovie.getTitle();
            Overview = modelMovie.getOverview();
            Thumbnail = modelMovie.getPosterPath();

            tvName.setText(NameFilm);
            tvOverview.setText(Overview);
            tvName.setSelected(true);

            Glide.with(this)
                    .load(ApiEndpoint.URLIMAGE + Thumbnail)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgPhoto);


        }

        imgFavorite.setOnFavoriteChangeListener(
                new MaterialFavoriteButton.OnFavoriteChangeListener() {
                    @Override
                    public void onFavoriteChanged(MaterialFavoriteButton buttonView, boolean favorite) {
                        if (favorite) {
                            Id = modelMovie.getId();
                            NameFilm = modelMovie.getTitle();
                            Overview = modelMovie.getOverview();
                            Thumbnail = modelMovie.getPosterPath();
                            helper.addFavoriteMovie(Id, NameFilm, Overview, Thumbnail);
                            Snackbar.make(buttonView, " Added to Favorite",
                                    Snackbar.LENGTH_SHORT).show();
                        } else {
                            helper.deleteFavoriteMovie(modelMovie.getId());
                            Snackbar.make(buttonView, " Removed from Favorite",
                                    Snackbar.LENGTH_SHORT).show();
                        }

                    }
                }
        );


    }
}
