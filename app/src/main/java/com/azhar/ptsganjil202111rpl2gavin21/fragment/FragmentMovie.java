package com.azhar.ptsganjil202111rpl2gavin21.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.azhar.ptsganjil202111rpl2gavin21.R;
import com.azhar.ptsganjil202111rpl2gavin21.activities.DetailMovieActivity;
import com.azhar.ptsganjil202111rpl2gavin21.adapter.MovieAdapter;
import com.azhar.ptsganjil202111rpl2gavin21.model.ModelMovie;
import com.azhar.ptsganjil202111rpl2gavin21.networking.ApiEndpoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FragmentMovie extends Fragment implements MovieAdapter.onSelectData {

    private RecyclerView rvFilmRecommend;
    private MovieAdapter movieAdapter;
    private List<ModelMovie> moviePopular = new ArrayList<>();

    public FragmentMovie() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_film, container, false);

        rvFilmRecommend = rootView.findViewById(R.id.rvFilmRecommend);
        rvFilmRecommend.setLayoutManager(new LinearLayoutManager(getActivity()));

        getMovie();

        return rootView;
    }

    private void getMovie() {
        AndroidNetworking.get(ApiEndpoint.BASEURL + ApiEndpoint.MOVIE_POPULAR + ApiEndpoint.APIKEY + ApiEndpoint.LANGUAGE)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            moviePopular = new ArrayList<>();
                            JSONArray Movie = response.getJSONArray("results");
                            for (int i = 0; i < Movie.length(); i++) {
                                JSONObject jsonObject = Movie.getJSONObject(i);
                                ModelMovie dataApi = new ModelMovie();

                                dataApi.setId(jsonObject.getInt("id"));
                                dataApi.setTitle(jsonObject.getString("title"));
                                dataApi.setOverview(jsonObject.getString("overview"));
                                dataApi.setPosterPath(jsonObject.getString("poster_path"));
                                moviePopular.add(dataApi);
                                showMovie();
                            }
                        } catch (JSONException e) {
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                    }
                });
    }

    private void showMovie() {
        movieAdapter = new MovieAdapter(getActivity(), moviePopular, this);
        rvFilmRecommend.setAdapter(movieAdapter);
    }

    @Override
    public void onSelected(ModelMovie modelMovie) {
        Intent intent = new Intent(getActivity(), DetailMovieActivity.class);
        intent.putExtra("detailMovie", modelMovie);
        startActivity(intent);
    }
}