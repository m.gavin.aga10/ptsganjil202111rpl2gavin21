package com.azhar.ptsganjil202111rpl2gavin21.realm;

import android.content.Context;

import com.azhar.ptsganjil202111rpl2gavin21.model.ModelMovie;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;


public class RealmHelper {

    private Context mContext;
    private Realm realm;
    private RealmResults<ModelMovie> modelMovie;

    public RealmHelper(Context mContext) {
        this.mContext = mContext;
        Realm.init(mContext);
        realm = Realm.getDefaultInstance();
    }

    public ArrayList<ModelMovie> showFavoriteMovie() {
        ArrayList<ModelMovie> data = new ArrayList<>();
        modelMovie = realm.where(ModelMovie.class).findAll();

        if (modelMovie.size() > 0) {
            for (int i = 0; i < modelMovie.size(); i++) {
                ModelMovie movie = new ModelMovie();
                movie.setId(modelMovie.get(i).getId());
                movie.setTitle(modelMovie.get(i).getTitle());
                movie.setOverview(modelMovie.get(i).getOverview());
                movie.setPosterPath(modelMovie.get(i).getPosterPath());
                data.add(movie);
            }
        }
        return data;
    }


    public void addFavoriteMovie(int Id, String Title, String Overview,
                             String PosterPath) {
        ModelMovie movie = new ModelMovie();
        movie.setId(Id);
        movie.setTitle(Title);
        movie.setOverview(Overview);
        movie.setPosterPath(PosterPath);

        realm.beginTransaction();
        realm.copyToRealm(movie);
        realm.commitTransaction();
    }


    public void deleteFavoriteMovie(int id) {
        realm.beginTransaction();
        RealmResults<ModelMovie> modelMovie = realm.where(ModelMovie.class).findAll();
        modelMovie.deleteAllFromRealm();
        realm.commitTransaction();

    }

}
